<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notes extends Model
{
    use HasFactory;

    protected $table = 'notes';
    protected $fillable = ['name', 'tags', 'description', 'owner'];
    // protected $hidden = ['id', 'created_at', 'updated_at'];
}
