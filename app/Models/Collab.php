<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Collab extends Model
{
    use HasFactory;

    protected $table = 'collabs';
    protected $fillable = ['notes_id', 'collaborator_id'];
    protected $hidden = [ 'created_at', 'updated_at'];
}
