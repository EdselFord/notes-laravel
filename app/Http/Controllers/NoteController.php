<?php

namespace App\Http\Controllers;

use App\Models\Collab;
use App\Models\Notes;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;

class NoteController extends Controller
{
    public function index(?int $note_id = null)
    {
        $id = Auth::id();
        $notes = Notes::select("notes.id as id", "name", "tags", "description", "owner", "notes.created_at", "notes.updated_at")
            ->leftJoin("collabs", "collabs.notes_id", "=", "notes.id");

        if ($note_id != null) {
            $notes = $notes->where('notes.id', $note_id)
                ->where(function ($query) use ($id) {
                    $query->where('notes.owner', $id)
                        ->orWhere('collabs.collaborator_id', $id);
                });
        } else {
            $notes = $notes->where("notes.owner", $id)->orWhere("collabs.collaborator_id", $id);
        }

        $note_result = $notes->get();

        if (empty($note_result->toArray()) && $note_id != null) return redirect(route('notes'));

        return Inertia::render($note_id == null ? 'Notes' : 'Display', [
            'notes' => $note_result,
            'owner' => $note_id != null ? DB::table('users')->where('id', $note_result[0]->owner)->first()->name : null,
            'collab' => array_map( function($data) {
                return [
                    'id' => $data['id'],
                    'collaborator_id' => $data['collaborator_id'],
                    'name' => DB::table('users')->where('id', $data['collaborator_id'])->first()->name
                ];
            }, Collab::where('notes_id', $note_id)->get()->toArray()),
            'type' => $note_id == null ? 'index' : 'update',
        ]);
    }

    public function create()
    {
        return Inertia::render('Display', ['notes' => [], 'type' => 'create']);
    }

    public function store(Request $request)
    {
        $type = $request->get('type');

        
        if ($type == 'create') {
            Notes::create($request->all());
        } else if ($type == 'update') {
            Notes::where('id', $request->id)->update([
                'name' => $request->get('name'),
                'tags' => $request->get('tags'),
                'description' => $request->get('description'),
                'owner' => $request->get('owner'),
            ]);
            return redirect('/notes/' . $request->get('id'));
        } else if ($type == 'delete') {

            // return response()->json('sedang menghapus data ' . $request->id);

            return response()->json(Notes::where('id', $request->id)->get());
        }

        return redirect('/notes');
    }

    public function destroy(int $id) {
        Notes::where('id', $id)->delete();
        Collab::where('notes_id', $id)->delete();
        return redirect('/notes');
    }

}
