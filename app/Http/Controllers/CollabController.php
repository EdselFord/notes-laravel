<?php

namespace App\Http\Controllers;

use App\Models\Collab;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CollabController extends Controller
{
    public function api(Request $request) {
        if ($request->search != null) {
            $users = DB::table('users')->where('name', 'like', '%' . $request->search . '%')->orWhere('email', 'like', '%' . $request->search . '%')->get();
            return response()->json(array_map(function ($user) {
                return [
                    'id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email
                ];
            }, $users->toArray()));
        }
        if ($request->add != null) {
            Collab::create(['notes_id' => $request->note, 'collaborator_id' => $request->add]);
    
            return redirect('/notes/' . $request->note);
        }
        if ($request->remove != null) {
            $collab = Collab::where('id', $request->remove);
            $note_id = $collab->first()->notes_id;
            $collab->delete();
            return redirect('/notes/' . $note_id);
        }
    }
}
