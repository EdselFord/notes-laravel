import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import { Head, Link, router } from "@inertiajs/react";

export default function Notes({ auth, notes }) {
    return (
        <AuthenticatedLayout user={auth.user}>
            <Head title="Notes" />

            {notes.length == 0 ? (
                <div className="h-[90vh] w-full flex items-center justify-center">
                    Notes are empty
                </div>
            ) : (
                <div className="py-12">
                    <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                        <div className="grid grid-cols-3 gap-4">
                            {notes.map((note, index) => (
                                <div
                                    key={index}
                                    onClick={() =>
                                        router.visit(route("notes", note.id))
                                    }
                                    className="bg-white overflow-hidden shadow-sm sm:rounded-lg p-6 h-64 cursor-pointer"
                                >
                                    <p className="text-lg">{note.name}</p>
                                    {note.tags.split(",").map((tag) => {
                                        return (
                                            <span className="inline-block bg-gray-200 rounded-full px-2 mr-1 mb-5 text-sm font-semibold text-gray-700">
                                                {tag}
                                            </span>
                                        );
                                    })}
                                    <div className="text-sm w-full h-full whitespace-pre-wrap break-words text-ellipsis line-clamp-[8]">
                                        {note.description}
                                    </div>
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            )}
        </AuthenticatedLayout>
    );
}
