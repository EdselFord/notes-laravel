import AuthenticatedLayout from "@/Layouts/AuthenticatedLayout";
import { Head, useForm } from "@inertiajs/react";
import PrimaryButton from "@/Components/PrimaryButton";
import { Toaster, toast } from "react-hot-toast";
import { useEffect, useState } from "react";
import DangerButton from "@/Components/DangerButton";
import axios from "axios";
import { Link } from "@inertiajs/react";

export default function Notes({ auth, notes, type, collab, owner }) {
    const [users, setUsers] = useState([]);
    const {
        data,
        setData,
        post,
        delete: drop,
        recentlySuccessful,
    } = useForm(
        notes.length == 0
            ? {
                  name: "",
                  tags: "",
                  description: "",
                  owner: auth.user.id,
                  type: type,
              }
            : { ...notes[0], type: type }
    );

    useEffect(() => {
        if (recentlySuccessful) {
            toast.success("Note saved");
        }
    }, [recentlySuccessful]);

    function indentation(e) {
        if (e.key == "Tab") {
            e.preventDefault();
            var start = e.currentTarget.selectionStart;
            var end = e.currentTarget.selectionEnd;

            e.currentTarget.value =
                e.currentTarget.value.substring(0, start) +
                "\t" +
                e.currentTarget.value.substring(end);

            e.currentTarget.selectionStart = e.currentTarget.selectionEnd =
                start + 1;
        }
    }

    function changeHandler(e, key) {
        setData(key, e.target.value);
    }

    function submitHandler() {
        post(route("notes"));
    }

    return (
        <AuthenticatedLayout user={auth.user}>
            <Head title="Notes" />

            {(function () {
                if (notes.length != 0 || type == "create") {
                    return (
                        <>
                            <div className="p-20 flex flex-col">
                                <input
                                    className="border-none bg-transparent focus:ring-0 text-4xl font-bold"
                                    type="text"
                                    onChange={(e) => changeHandler(e, "name")}
                                    value={data.name}
                                    placeholder="Title"
                                />
                                {type == "update" && (
                                    <>
                                        <p className="text-sm mx-3 opacity-70">
                                            Owned by {owner}
                                        </p>
                                        <p className="text-sm mx-3 opacity-70">
                                            Created at{" "}
                                            {new Date(
                                                data.created_at
                                            ).toLocaleDateString("en-US", {
                                                weekday: "short",
                                                month: "short",
                                                day: "numeric",
                                                year: "numeric",
                                            })}
                                        </p>
                                    </>
                                )}
                                <input
                                    className="border-none bg-transparent focus:ring-0"
                                    type="text"
                                    value={data.tags}
                                    onChange={(e) => changeHandler(e, "tags")}
                                    placeholder="tags1, tags2, tags3"
                                />
                                <textarea
                                    className="p-3 h-[50vh]  focus:outline-none border-none bg-transparent focus:ring-0"
                                    onKeyDown={indentation}
                                    onChange={(e) =>
                                        changeHandler(e, "description")
                                    }
                                    value={data.description}
                                />
                                {type == "update" &&
                                    data.owner == auth.user.id && (
                                        <div>
                                            <div>
                                                <h1 className="inline">
                                                    Collaboration:
                                                </h1>
                                                {collab.map((user) => (
                                                    <p
                                                        key={user.id}
                                                        className="inline mx-5"
                                                    >
                                                        {user.name}{" "}
                                                        <Link
                                                            href={
                                                                route(
                                                                    "api.collab"
                                                                ) +
                                                                `?remove=${user.id}`
                                                            }
                                                        >
                                                            <svg
                                                                className="inline"
                                                                xmlns="http://www.w3.org/2000/svg"
                                                                viewBox="0 -960 960 960"
                                                                width="24"
                                                            >
                                                                <path d="M280-120q-33 0-56.5-23.5T200-200v-520h-40v-80h200v-40h240v40h200v80h-40v520q0 33-23.5 56.5T680-120H280Zm400-600H280v520h400v-520ZM360-280h80v-360h-80v360Zm160 0h80v-360h-80v360ZM280-720v520-520Z" />
                                                            </svg>
                                                        </Link>
                                                    </p>
                                                ))}
                                            </div>
                                            <input
                                                className="w-52"
                                                type="text"
                                                placeholder="Search user"
                                                onChange={(e) =>
                                                    e.target.value == ""
                                                        ? setUsers([])
                                                        : axios
                                                              .get(
                                                                  route(
                                                                      "api.collab"
                                                                  ) +
                                                                      "?search=" +
                                                                      e.target
                                                                          .value
                                                              )
                                                              .then((res) =>
                                                                  setUsers(
                                                                      res.data.filter(
                                                                          (
                                                                              user
                                                                          ) =>
                                                                              user.id !=
                                                                              auth
                                                                                  .user
                                                                                  .id
                                                                      )
                                                                  )
                                                              )
                                                }
                                            />
                                            <button className="rounded bg-black text-white py-2 px-4 mx-2">
                                                Add
                                            </button>
                                            <div className="bg-slate-200 w-52 ">
                                                {users.map((user) => (
                                                    <Link
                                                        key={user.id}
                                                        href={
                                                            route(
                                                                "api.collab"
                                                            ) +
                                                            `?note=${data.id}&add=${user.id}`
                                                        }
                                                    >
                                                        <div className="hover:bg-slate-300 p-3 border border-black border-t-0">
                                                            {user.name}
                                                        </div>
                                                    </Link>
                                                ))}
                                            </div>
                                        </div>
                                    )}
                            </div>
                            <div className="fixed bottom-5 right-5">
                                {type == "update" &&
                                    data.owner == auth.user.id && (
                                        <DangerButton
                                            onClick={() => {
                                                drop(route("notes", data.id));
                                            }}
                                        >
                                            Delete
                                        </DangerButton>
                                    )}
                                <PrimaryButton
                                    onClick={submitHandler}
                                    className="mx-1"
                                >
                                    Save
                                </PrimaryButton>
                            </div>

                            <Toaster />
                        </>
                    );
                }

                return (
                    <div className="w-full h-[90vh] flex items-center justify-center">
                        <h1>Notes not found</h1>
                    </div>
                );
            })()}
        </AuthenticatedLayout>
    );
}
